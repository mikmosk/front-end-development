function initCellsState(){
	return document.querySelectorAll('#field td');
}
function fillCell(){
	let counter = 0;
	let gamers = ['X', 'O'];
	let result;
	let cells = initCellsState();
	for(let cell of cells){
		cell.addEventListener('click', function(){
			if(cell.innerHTML == "") {
				this.innerHTML = gamers[counter % 2];			
			} 
			if(cells[0].innerHTML == gamers[counter % 2] && cells[1].innerHTML == gamers[counter % 2] && cells[2].innerHTML == gamers[counter % 2] ||
				cells[3].innerHTML == gamers[counter % 2] && cells[4].innerHTML == gamers[counter % 2] && cells[5].innerHTML == gamers[counter % 2] ||	
				cells[6].innerHTML == gamers[counter % 2] && cells[7].innerHTML == gamers[counter % 2] && cells[8].innerHTML == gamers[counter % 2] ||	
				cells[0].innerHTML == gamers[counter % 2] && cells[3].innerHTML == gamers[counter % 2] && cells[6].innerHTML == gamers[counter % 2] ||	
				cells[1].innerHTML == gamers[counter % 2] && cells[4].innerHTML == gamers[counter % 2] && cells[7].innerHTML == gamers[counter % 2] ||	
				cells[2].innerHTML == gamers[counter % 2] && cells[5].innerHTML == gamers[counter % 2] && cells[8].innerHTML == gamers[counter % 2] ||	
				cells[2].innerHTML == gamers[counter % 2] && cells[4].innerHTML == gamers[counter % 2] && cells[6].innerHTML == gamers[counter % 2] ||	
				cells[0].innerHTML == gamers[counter % 2] && cells[4].innerHTML == gamers[counter % 2] && cells[8].innerHTML == gamers[counter % 2] ) {				
					alert("Winner: " + gamers[counter % 2]);	
					location.reload();
			} else if (counter == 8) {
				alert("Draw");
				location.reload();
			}
			counter++;				
		});		
	}		
	return cells;
}

fillCell();