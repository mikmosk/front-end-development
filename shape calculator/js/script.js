function visibilityController() {
	let defaultInput = document.getElementById("input-square");
	defaultInput.style.display = "block";

	let inputs = document.querySelectorAll("#input-area > div");
	let radios = document.inputForm.inputs;	

	for (let i = 0; i < radios.length; i++) {				
		radios[i].addEventListener("change", function(event){
			for (let i = 0; i < radios.length; i++) {
				inputs[i].style.display = "none";	
			}			
			if (event.returnValue) {				
				inputs[i].style.display = "block";							
			}				
		});		
	}	
}
function calculateHandler(){
	let radios = document.inputForm.inputs;	
	let activeRadio;
	for(let i = 0; i < radios.length; i++){
		if(radios[i].checked) {
			activeInput = radios[i].value;			  
		}
	}
	if(activeInput == "square"){
		let squareSide = document.getElementById("square-side").value;
		alert("Area of Square: " + squareSide*squareSide + ", Perimeter of Square: " + squareSide*4);
	}
	if(activeInput == "rectangle"){
		let rectangleSideA = document.getElementById("rectangle-side-a").value;
		let rectangleSideB = document.getElementById("rectangle-side-b").value;
		if(rectangleSideA > 0 && rectangleSideB > 0) {
			alert("Area of Rectangle: " + rectangleSideA*rectangleSideB + ", Perimeter of Rectangle: " + (+rectangleSideA + +rectangleSideB)*2);
		}
	}
	if(activeInput == "circle"){		
		let circleRadius = document.getElementById("circle-radius").value;
		if(circleRadius > 0) {
			alert("Area of circle: " + Math.PI*circleRadius*circleRadius + ", Circuit: " + 2*Math.PI*circleRadius);
		}
	}
	if(activeInput == "triangle"){
		let triangleSideA = document.getElementById("triangle-side-a").value;
		let triangleSideB = document.getElementById("triangle-side-b").value;
		let triangleSideC = document.getElementById("triangle-side-c").value;

		if((triangleSideA + triangleSideB > triangleSideC) && (triangleSideA + triangleSideC > triangleSideB) && (triangleSideB + triangleSideC > triangleSideA)) {
			let p = (+triangleSideA + +triangleSideB + +triangleSideC)/2;			
			let area = Math.sqrt(p*(p-triangleSideA)*(p-triangleSideB)*(p-triangleSideC));
			alert("Area of triangle: " + area);
		} else alert("It's NOT a triangle");
	}
}


(function init(){
	visibilityController();
	let btn = document.getElementById("calculate-button");
	btn.addEventListener("click", calculateHandler);
})();